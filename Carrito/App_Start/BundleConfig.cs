﻿using System.Web;
using System.Web.Optimization;

namespace Carrito
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/theme/js/jquery.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/theme").Include(
                        "~/Scripts/theme/js/jquery.cookie.js",
                        "~/Scripts/theme/js/waypoints.js",
                        "~/Scripts/theme/js/modernizr.js",
                        "~/Scripts/theme/js/bootstrap-hover-dropdown.js",
                        "~/Scripts/theme/js/owl.carousel.js",
                        "~/Scripts/theme/js/front.js"        
                        ));
            bundles.Add(new StyleBundle("~/Content/css/theme").Include(
                "~/Scripts/theme/css/font-awesome.css",
                "~/Scripts/theme/css/bootstrap.css",
                "~/Scripts/theme/css/animate.css",
                "~/Scripts/theme/css/owl.carousel.css",
                "~/Scripts/theme/css/owl.theme.css",
                "~/Scripts/theme/css/style.default.css"
                ));
        }
    }
}
