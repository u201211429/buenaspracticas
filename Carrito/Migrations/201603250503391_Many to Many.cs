namespace Carrito.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ManytoMany : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Productoes", "Carrito_Id", "dbo.Carritoes");
            DropIndex("dbo.Productoes", new[] { "Carrito_Id" });
            CreateTable(
                "dbo.ProductoCarritoes",
                c => new
                    {
                        Producto_Id = c.Int(nullable: false),
                        Carrito_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Producto_Id, t.Carrito_Id })
                .ForeignKey("dbo.Productoes", t => t.Producto_Id, cascadeDelete: true)
                .ForeignKey("dbo.Carritoes", t => t.Carrito_Id, cascadeDelete: true)
                .Index(t => t.Producto_Id)
                .Index(t => t.Carrito_Id);
            
            DropColumn("dbo.Productoes", "Carrito_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Productoes", "Carrito_Id", c => c.Int());
            DropForeignKey("dbo.ProductoCarritoes", "Carrito_Id", "dbo.Carritoes");
            DropForeignKey("dbo.ProductoCarritoes", "Producto_Id", "dbo.Productoes");
            DropIndex("dbo.ProductoCarritoes", new[] { "Carrito_Id" });
            DropIndex("dbo.ProductoCarritoes", new[] { "Producto_Id" });
            DropTable("dbo.ProductoCarritoes");
            CreateIndex("dbo.Productoes", "Carrito_Id");
            AddForeignKey("dbo.Productoes", "Carrito_Id", "dbo.Carritoes", "Id");
        }
    }
}
