namespace Carrito.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Carritoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Compras",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        carrito_Id = c.Int(),
                        usuario_Id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Carritoes", t => t.carrito_Id)
                .ForeignKey("dbo.Usuarios", t => t.usuario_Id)
                .Index(t => t.carrito_Id)
                .Index(t => t.usuario_Id);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Compras", "usuario_Id", "dbo.Usuarios");
            DropForeignKey("dbo.Compras", "carrito_Id", "dbo.Carritoes");
            DropIndex("dbo.Compras", new[] { "usuario_Id" });
            DropIndex("dbo.Compras", new[] { "carrito_Id" });
            DropTable("dbo.Usuarios");
            DropTable("dbo.Compras");
            DropTable("dbo.Carritoes");
        }
    }
}
