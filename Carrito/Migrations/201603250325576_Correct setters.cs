namespace Carrito.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Correctsetters : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Carritoes", "usuario_Id", c => c.Int());
            AddColumn("dbo.Productoes", "Carrito_Id", c => c.Int());
            CreateIndex("dbo.Carritoes", "usuario_Id");
            CreateIndex("dbo.Productoes", "Carrito_Id");
            AddForeignKey("dbo.Productoes", "Carrito_Id", "dbo.Carritoes", "Id");
            AddForeignKey("dbo.Carritoes", "usuario_Id", "dbo.Usuarios", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Carritoes", "usuario_Id", "dbo.Usuarios");
            DropForeignKey("dbo.Productoes", "Carrito_Id", "dbo.Carritoes");
            DropIndex("dbo.Productoes", new[] { "Carrito_Id" });
            DropIndex("dbo.Carritoes", new[] { "usuario_Id" });
            DropColumn("dbo.Productoes", "Carrito_Id");
            DropColumn("dbo.Carritoes", "usuario_Id");
        }
    }
}
