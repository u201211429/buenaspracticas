namespace Carrito.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarDetalle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Productoes", "Detalle", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Productoes", "Detalle");
        }
    }
}
