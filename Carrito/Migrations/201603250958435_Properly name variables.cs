namespace Carrito.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Properlynamevariables : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Carritoes", new[] { "usuario_Id" });
            DropIndex("dbo.Compras", new[] { "carrito_Id" });
            DropIndex("dbo.Compras", new[] { "usuario_Id" });
            CreateIndex("dbo.Carritoes", "Usuario_Id");
            CreateIndex("dbo.Compras", "Carrito_Id");
            CreateIndex("dbo.Compras", "Usuario_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Compras", new[] { "Usuario_Id" });
            DropIndex("dbo.Compras", new[] { "Carrito_Id" });
            DropIndex("dbo.Carritoes", new[] { "Usuario_Id" });
            CreateIndex("dbo.Compras", "usuario_Id");
            CreateIndex("dbo.Compras", "carrito_Id");
            CreateIndex("dbo.Carritoes", "usuario_Id");
        }
    }
}
