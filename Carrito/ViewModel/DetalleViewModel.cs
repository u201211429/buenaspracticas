﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Carrito.ViewModel
{
    public class DetalleViewModel
    {
        public string Id { get; set;}

        public string Nombre { get; set; }

        [DataType(DataType.Currency)]
        public double PrecioUnitario { get; set; }

        public double Descuento { get; set; }

        public string Detalle { get; set; }
    }
}