﻿using Carrito.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrito.ViewModel
{
    public class IndexViewModel
    {
        public List<Producto> productos { get; set; }
        public string cantidadEnCarrito { get; set; }
    }
}