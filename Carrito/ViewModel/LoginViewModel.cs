﻿using Carrito.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrito.ViewModel
{
    public class LoginViewModel
    {
        public String Usuario { get; set; }
        public String Clave { get; set; }
    }
}