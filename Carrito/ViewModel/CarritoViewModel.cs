﻿using Carrito.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrito.ViewModel
{
    public class CarritoViewModel
    {
        public int Id { get; set; }
        public List<ProductoViewModel> Productos { get; set; }
        public string CantidadEnCarrito { get; set; }
        public double Total { get; set;}
    }
}