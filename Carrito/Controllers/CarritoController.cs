﻿using Carrito.Models;
using Carrito.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Carrito = Carrito.Models.Carrito;

namespace Carrito.Controllers
{
    public class CarritoController : Controller
    {
        // GET: Carrito
        public ActionResult Index(int id)
        {
            var entities = new Entities();

            // Decirle a EF que cargue la coleccion relacionada
            var carrito = entities.Carritos.Include(x => x.Productos).FirstOrDefault(x => x.Id == id);

            var model = new CarritoViewModel();

            if (carrito == null) { return RedirectToAction("Index", "Home"); }

            // Se puede simplificar con operadores de casteo
            var productos = carrito.Productos.Select(x => new ProductoViewModel
            {
                Id = x.Id,
                Descuento = x.Descuento,
                Nombre = x.Nombre,
                PrecioUnitario = x.PrecioUnitario,
                Total = x.PrecioUnitario * (1 - x.Descuento / 100)
            })
                .ToList();

            model.Productos = new List<ProductoViewModel>();
            model.Productos.AddRange(productos);
            model.CantidadEnCarrito = productos.Count().ToString();
            model.Total = productos.Sum(x => x.Total);
            model.Id = carrito.Id;

            return View(model);

        }

        public ActionResult Eliminar(int carritoId, int productoId)
        {
            using (var ctx = new Entities())
            {
                var carrito = ctx.Carritos.Include(x => x.Productos).FirstOrDefault(x => x.Id == carritoId);
                var producto = ctx.Productos.FirstOrDefault(x => x.Id == productoId);
                carrito?.Productos.Remove(producto);
                ctx.SaveChanges();
            }


            return RedirectToAction("Index",new {id = carritoId });
        }
    }
}