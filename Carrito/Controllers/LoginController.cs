﻿using Carrito.Models;
using Carrito.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Carrito.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            var model = new LoginViewModel();

            return View(model);
        }
        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                Console.Write(model.Clave);
                Console.Write(model.Usuario);
                var context = new Entities();
                var usuario =
                    context.Usuarios.FirstOrDefault(x => x.Nombre == model.Usuario && x.Password == model.Clave);
                if (usuario != null)
                {
                    DarCarrito(context, usuario);

                    Session.Add("Usuario", usuario.Id   );
                    Session.Add("Nombre", usuario.Nombre);
                    return RedirectToAction("Index", "Home");
                }
            }
            ModelState.AddModelError(string.Empty, "El usuario o contraseña son incorrectos");
            return View("Index", model);
        }

        private void DarCarrito(Entities context, Usuario usuario)
        {
            //Dar al usuario un carrito de comrpas
            var carrito =
                context.Carritos.Include(x => x.Productos).FirstOrDefault(x => x.Usuario.Id == usuario.Id );

            if (carrito == null)
            {
                var nuevoCarrito = context.Carritos.Add(new Models.Carrito() { Usuario = usuario });
                context.SaveChanges();
                Session.Add("Carrito", nuevoCarrito.Id);
            }
            else
            {
                Session.Add("Carrito", carrito.Id);
            }

        }
    }
}