﻿using Carrito.Models;
using Carrito.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Carrito.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            IndexViewModel viewModel;
            using (var ctx = new Entities())
            {
                viewModel = new IndexViewModel
                {
                    productos = ctx.Productos.ToList(),
                    cantidadEnCarrito = "0"
                };

            }            
            
            return View(viewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}