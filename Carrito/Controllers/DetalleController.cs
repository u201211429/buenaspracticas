﻿using Carrito.Models;
using Carrito.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Carrito.Controllers
{
    public class DetalleController : Controller
    {
        // GET: Detalle
        public ActionResult Index(int? id)
        {
            var model = new DetalleViewModel();

            using (var context = new Entities())
            {
                Producto producto = context.Productos.FirstOrDefault(x => x.Id == id);

                // Se puede reemplazar por un operadores explicitos, pero semana santa
                if (producto == null) return View(model);

                model.Nombre = producto.Nombre;
                model.Descuento = producto.Descuento;
                model.Detalle = producto.Detalle;
                model.PrecioUnitario = producto.PrecioUnitario;
                model.Id = producto.Id.ToString();
            }

            return View(model);
        }
        public ActionResult Agregar(int id)
        {
            using (Entities context = new Entities())
            {
                var producto = context.Productos.FirstOrDefault(x => x.Id == id);
                int idUsuario = Convert.ToInt16(Session["Usuario"]);
                Models.Carrito carrito = context.Carritos.Include(x => x.Productos).FirstOrDefault(x => x.Usuario.Id == idUsuario);

                // Si es la primera vez que se está comprando algo y por ende no tiene carrito asociado
                if (carrito == null)
                {

                    var usuario = context.Usuarios.FirstOrDefault(x => x.Id == idUsuario);
                    var productos = new List<Producto> { producto };

                    var nuevoCarrito = context.Carritos.Add(new Models.Carrito() { Usuario = usuario, Productos = productos });
                    nuevoCarrito.Productos.Add(producto);
                    context.SaveChanges();
                    return RedirectToAction("Index", "Carrito", new { id = nuevoCarrito.Id });
                }
                // Si ya tiene un carrito
                carrito.Productos.Add(producto);
                context.SaveChanges();
                return RedirectToAction("Index", "Carrito", new { id = carrito.Id });

            }

        }
    }
}