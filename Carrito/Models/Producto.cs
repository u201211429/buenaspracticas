﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Carrito.Models
{
    public class Producto
    {
        [Key]
        public int Id { get; set; }

        public string Nombre { get; set; }

        [DataType(DataType.Currency)]
        public double PrecioUnitario { get; set; }

        public double Descuento { get; set; }

        public string Detalle { get; set; }

        public List<Carrito> Carritos { get; set; }
    }
}