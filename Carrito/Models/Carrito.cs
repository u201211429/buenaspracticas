﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Carrito.Models
{
    public class Carrito
    {
        [Key]
        public int Id { get; set; }

        public Usuario Usuario { get; set; }

        public List<Producto> Productos { get; set; }
    }
}