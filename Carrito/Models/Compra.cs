﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Carrito.Models
{
    public class Compra
    {
        [Key]
        public int Id { get; set; }
        public Carrito Carrito { get; set; }
        public Usuario Usuario { get; set; }
    }
}