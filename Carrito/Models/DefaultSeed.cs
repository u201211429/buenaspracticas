﻿using System.Collections.Generic;
using System.Data.Entity;


namespace Carrito.Models
{
    public class DefaultSeed : DropCreateDatabaseIfModelChanges<Entities>
    {
        protected override void Seed(Entities entities)
        {
            List<Usuario> defaultUsuarios = new List<Usuario>();
            List<Producto> defaultProductos = new List<Producto>();

            defaultUsuarios.Add(new Usuario() { Nombre = "jose", Password = "jose" });
            defaultUsuarios.Add(new Usuario() { Nombre = "jamil", Password = "jamil" });
            defaultProductos.Add(new Producto() { Nombre = "Jugo", PrecioUnitario = 30, Descuento = 0,Detalle = "El mejor jugo que se puede tomar" });
            defaultProductos.Add(new Producto() { Nombre = "Carro", PrecioUnitario = 20000, Descuento = 30,Detalle =  "El mejor carro que se puede manejar" });
            defaultProductos.Add(new Producto() { Nombre = "Polo", PrecioUnitario = 15, Descuento = 0, Detalle = "El mejor jugo polo que se puede usar" });
            defaultProductos.Add(new Producto() { Nombre = "Pantalon", PrecioUnitario = 30 , Descuento = 0, Detalle = "El mejor pantalon que se puede usar"});
            defaultProductos.Add(new Producto() { Nombre = "Chompa", PrecioUnitario = 30, Descuento = 0, Detalle = "La mejor chompa que se puede usar" });

            entities.Usuarios.AddRange(defaultUsuarios);
            entities.Productos.AddRange(defaultProductos);

            entities.SaveChanges();
        }
    }
}